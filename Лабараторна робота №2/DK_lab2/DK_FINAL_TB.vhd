-- Vhdl test bench created from schematic D:\Xilinx\DK_lab2\DK_SCH_ALL.sch - Sun Sep 18 17:27:34 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY DK_SCH_ALL_DK_SCH_ALL_sch_tb IS
END DK_SCH_ALL_DK_SCH_ALL_sch_tb;
ARCHITECTURE behavioral OF DK_SCH_ALL_DK_SCH_ALL_sch_tb IS 

   COMPONENT DK_SCH_ALL
   PORT( CLC	:	IN	STD_LOGIC; 
          DK_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          DK_B	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          DK_PROD	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          DK_mPROD	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          DK_IP_PROD	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          DK_CM	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0); 
          DK_CmM	:	OUT	STD_LOGIC_VECTOR (39 DOWNTO 0); 
          DK_IP_CM	:	OUT	STD_LOGIC_VECTOR (38 DOWNTO 0));
   END COMPONENT;

   SIGNAL CLC	:	STD_LOGIC;
   SIGNAL DK_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL DK_B	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL DK_PROD	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL DK_mPROD	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL DK_IP_PROD	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL DK_CM	:	STD_LOGIC_VECTOR (39 DOWNTO 0);
   SIGNAL DK_CmM	:	STD_LOGIC_VECTOR (39 DOWNTO 0);
   SIGNAL DK_IP_CM	:	STD_LOGIC_VECTOR (38 DOWNTO 0);
	constant clk_c : time := 10 ns;
		SIGNAL  buf:  integer := 0;
BEGIN

   UUT: DK_SCH_ALL PORT MAP(
		CLC => CLC, 
		DK_A => DK_A, 
		DK_B => DK_B, 
		DK_PROD => DK_PROD, 
		DK_mPROD => DK_mPROD, 
		DK_IP_PROD => DK_IP_PROD, 
		DK_CM => DK_CM, 
		DK_CmM => DK_CmM, 
		DK_IP_CM => DK_IP_CM
   );
CLC_process :process

	begin
	DK_A <= "00000001";
	DK_B <= "00000010";
	-- �������������� ��� �����
	--buf <= buf + 1;
	
	CLC <= '0';
	wait for clk_c/2;
	-- �������������� ��� �����
	-- DK_A <= std_logic_vector(unsigned(DK_A) + (buf));
--
	CLC <= '1';
	wait for clk_c/2;
	--
--DK_A <= std_logic_vector(unsigned(DK_A) + (buf));
	end process;
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
