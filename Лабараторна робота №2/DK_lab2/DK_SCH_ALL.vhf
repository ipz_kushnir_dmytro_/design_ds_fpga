--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : DK_SCH_ALL.vhf
-- /___/   /\     Timestamp : 09/20/2016 08:10:19
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -sympath D:/Xilinx/DK_lab2/ipcore_dir -intstyle ise -family virtex7 -flat -suppress -vhdl D:/Xilinx/DK_lab2/DK_SCH_ALL.vhf -w D:/Xilinx/DK_lab2/DK_SCH_ALL.sch
--Design Name: DK_SCH_ALL
--Device: virtex7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity DK_SCH_ALL is
   port ( DK_A       : in    std_logic_vector (7 downto 0); 
          DK_B       : in    std_logic_vector (7 downto 0); 
          DK_CM      : out   std_logic_vector (39 downto 0); 
          DK_CmM     : out   std_logic_vector (39 downto 0); 
          DK_IP_CM   : out   std_logic_vector (38 downto 0); 
          DK_IP_PROD : out   std_logic_vector (15 downto 0); 
          DK_mPROD   : out   std_logic_vector (15 downto 0); 
          DK_PROD    : out   std_logic_vector (15 downto 0));
end DK_SCH_ALL;

architecture BEHAVIORAL of DK_SCH_ALL is
   component DK_Multiplier2
      port ( DK_A     : in    std_logic_vector (7 downto 0); 
             DK_B     : in    std_logic_vector (7 downto 0); 
             DK_mPROD : out   std_logic_vector (15 downto 0));
   end component;
   
   component DK_Multiplier1
      port ( DK_A    : in    std_logic_vector (7 downto 0); 
             DK_B    : in    std_logic_vector (7 downto 0); 
             DK_PROD : out   std_logic_vector (15 downto 0));
   end component;
   
   component NotOptimized
      port ( DK_A   : in    std_logic_vector (7 downto 0); 
             DK_C_M : out   std_logic_vector (39 downto 0));
   end component;
   
   component Optimized
      port ( DK_A   : in    std_logic_vector (7 downto 0); 
             DK_Opt : out   std_logic_vector (39 downto 0));
   end component;
   
   component DC_C_IP_MULT
      port ( a : in    std_logic_vector (7 downto 0); 
             p : out   std_logic_vector (38 downto 0));
   end component;
   
   component DC_IP_MULTIPLIER
      port ( a : in    std_logic_vector (7 downto 0); 
             b : in    std_logic_vector (7 downto 0); 
             p : out   std_logic_vector (15 downto 0));
   end component;
   
begin
   XLXI_10 : DK_Multiplier2
      port map (DK_A(7 downto 0)=>DK_A(7 downto 0),
                DK_B(7 downto 0)=>DK_B(7 downto 0),
                DK_mPROD(15 downto 0)=>DK_mPROD(15 downto 0));
   
   XLXI_13 : DK_Multiplier1
      port map (DK_A(7 downto 0)=>DK_A(7 downto 0),
                DK_B(7 downto 0)=>DK_B(7 downto 0),
                DK_PROD(15 downto 0)=>DK_PROD(15 downto 0));
   
   XLXI_14 : NotOptimized
      port map (DK_A(7 downto 0)=>DK_A(7 downto 0),
                DK_C_M(39 downto 0)=>DK_CM(39 downto 0));
   
   XLXI_15 : Optimized
      port map (DK_A(7 downto 0)=>DK_A(7 downto 0),
                DK_Opt(39 downto 0)=>DK_CmM(39 downto 0));
   
   XLXI_17 : DC_C_IP_MULT
      port map (a(7 downto 0)=>DK_A(7 downto 0),
                p(38 downto 0)=>DK_IP_CM(38 downto 0));
   
   XLXI_18 : DC_IP_MULTIPLIER
      port map (a(7 downto 0)=>DK_A(7 downto 0),
                b(7 downto 0)=>DK_B(7 downto 0),
                p(15 downto 0)=>DK_IP_PROD(15 downto 0));
   
end BEHAVIORAL;


