library IEEE;

use IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_ARITH.ALL;

use IEEE.STD_LOGIC_UNSIGNED.ALL;

use IEEE.numeric_std.all;

LIBRARY UNISIM;

USE UNISIM.Vcomponents.ALL;

-- Uncomment the following library declaration if using

-- arithmetic functions with Signed or Unsigned values

--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating

-- any Xilinx primitives in this code.

--library UNISIM;

--use UNISIM.VComponents.all;

entity AMB_const is

Port ( AMB_A : in STD_LOGIC_VECTOR (31 downto 0);

AMB_REZ_CONST : out STD_LOGIC_VECTOR (63 downto 0);

CLK : in STD_LOGIC);

end AMB_const;

architecture Behavioral of AMB_const is

--error

signal AMB_prod :std_logic_vector(63 downto 0);

signal AMB_p0 : std_logic_vector(63 downto 0);

signal AMB_p1 : std_logic_vector(63 downto 0);

signal AMB_p2 : std_logic_vector(63 downto 0);

signal AMB_p3 : std_logic_vector(63 downto 0);

signal AMB_p4 : std_logic_vector(63 downto 0);

signal AMB_p5 : std_logic_vector(63 downto 0);

signal AMB_p6 : std_logic_vector(63 downto 0);

signal AMB_p7 : std_logic_vector(63 downto 0);

signal AMB_p8 : std_logic_vector(63 downto 0);

signal AMB_p9 : std_logic_vector(63 downto 0);

signal AMB_p10 : std_logic_vector(63 downto 0);

signal AMB_p11 : std_logic_vector(63 downto 0);

signal AMB_p12 : std_logic_vector(63 downto 0);

signal AMB_p13 : std_logic_vector(63 downto 0);

signal AMB_p14 : std_logic_vector(63 downto 0);

signal AMB_p15 : std_logic_vector(63 downto 0);

--signal AMB_p16 : std_logic_vector(63 downto 0);

signal AMB_p17 : std_logic_vector(63 downto 0);

--signal AMB_p18 : std_logic_vector(63 downto 0);

signal AMB_p19 : std_logic_vector(63 downto 0);

--signal AMB_p20 : std_logic_vector(63 downto 0);

--signal AMB_p21 : std_logic_vector(63 downto 0);

--signal AMB_p22 : std_logic_vector(63 downto 0);

signal AMB_p23 : std_logic_vector(63 downto 0);

--signal AMB_p24 : std_logic_vector(63 downto 0);

--signal AMB_p25 : std_logic_vector(63 downto 0);

--signal AMB_p26 : std_logic_vector(63 downto 0);

--signal AMB_p27 : std_logic_vector(63 downto 0);

begin

AMB_p0 <= "00000000000000000000000000000000" & (AMB_A);

AMB_p1 <= "0000000000000000000000000000000" & (AMB_A) & "0";

AMB_p2 <= "000000000000000000000000000000" & (AMB_A) & "00";

AMB_p3 <= "00000000000000000000000000000" & (AMB_A) & "000";

AMB_p4 <= "0000000000000000000000000000" & (AMB_A) & "0000";

AMB_p5 <= "000000000000000000000000000" & (AMB_A) & "00000";

AMB_p6 <= "00000000000000000000000000" & (AMB_A) & "000000";

AMB_p7 <= "0000000000000000000000000" & (AMB_A) & "0000000";

AMB_p8 <= "000000000000000000000000" & (AMB_A) & "00000000";

AMB_p9 <= "00000000000000000000000" & (AMB_A) & "000000000";

AMB_p10 <= "0000000000000000000000" & (AMB_A) & "0000000000";

AMB_p11 <= "000000000000000000000" & (AMB_A) & "00000000000";

AMB_p12 <= "00000000000000000000" & (AMB_A) & "000000000000";

AMB_p13 <= "0000000000000000000" & (AMB_A) & "0000000000000";

AMB_p14 <= "000000000000000000" & (AMB_A) & "00000000000000";

AMB_p15 <= "00000000000000000" & (AMB_A) & "000000000000000";

AMB_p17 <= "000000000000000" & (AMB_A) & "00000000000000000";

AMB_p19 <= "0000000000000" & (AMB_A) & "0000000000000000000";

AMB_p23 <= "000000000" & (AMB_A) & "00000000000000000000000";

AMB_prod <= AMB_p0+AMB_p1+AMB_p2+AMB_p3+AMB_p4+AMB_p5+AMB_p6+AMB_p7+AMB_p8+AMB_p9+AMB_p10+AMB_p11+AMB_p12+AMB_p13+AMB_p14+AMB_p15+AMB_p17+AMB_p19+AMB_p23;

AMB_REZ_CONST <= std_logic_vector (AMB_prod);

end Behavioral;