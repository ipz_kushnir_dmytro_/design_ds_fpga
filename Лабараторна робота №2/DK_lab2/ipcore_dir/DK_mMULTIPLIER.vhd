----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:27:06 09/17/2016 
-- Design Name: 
-- Module Name:    DK_mMULTIPLIER - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use IEEE.NUMERIC_STD.ALL;
entity DK_SCH_ALL is
	Port ( DK_A : in STD_LOGIC_VECTOR (7 downto 0);
	DK_B : in STD_LOGIC_VECTOR (7 downto 0);
	DK_mPROD : out STD_LOGIC_VECTOR (15 downto 0));
end DK_SCH_ALL;
architecture Behavioral of DK_SCH_ALL is
	constant WIDTH: integer:=8;
	signal dk_ua, dk_bv0, dk_bv1, dk_bv2, dk_bv3, dk_bv4, dk_bv5, dk_bv6, dk_bv7 : unsigned (WIDTH - 1 downto 0);
	signal dk_p0, dk_p1, dk_p2, dk_p3, dk_p4, dk_p5, dk_p6, dk_p7: unsigned (WIDTH downto 0);
	signal dk_product: unsigned (2*WIDTH - 1 downto 0);
begin
	dk_ua <= unsigned (DK_A);
	dk_bv0 <= (others => DK_B(0));
	dk_bv1 <= (others => DK_B(1));
	dk_bv2 <= (others => DK_B(2));
	dk_bv3 <= (others => DK_B(3));
	dk_bv4 <= (others => DK_B(4));
	dk_bv5 <= (others => DK_B(5));
	dk_bv6 <= (others => DK_B(6));
	dk_bv7 <= (others => DK_B(7));
	dk_p0 <= "0" & (dk_bv0 and dk_ua);
	dk_p1 <= ("0" & dk_p0(WIDTH DOWNTO 1)) + ("0" & (dk_bv1 and dk_ua));
	dk_p2 <= ("0" & dk_p1(WIDTH DOWNTO 1)) + ("0" & (dk_bv2 and dk_ua));
	dk_p3 <= ("0" & dk_p2(WIDTH DOWNTO 1)) + ("0" & (dk_bv3 and dk_ua));
	dk_p4 <= ("0" & dk_p3(WIDTH DOWNTO 1)) + ("0" & (dk_bv4 and dk_ua));
	dk_p5 <= ("0" & dk_p4(WIDTH DOWNTO 1)) + ("0" & (dk_bv5 and dk_ua));
	dk_p6 <= ("0" & dk_p5(WIDTH DOWNTO 1)) + ("0" & (dk_bv6 and dk_ua));
	dk_p7 <= ("0" & dk_p6(WIDTH DOWNTO 1)) + ("0" & (dk_bv7 and dk_ua));
	dk_product <= dk_p7 & dk_p6(0) & dk_p5(0)& dk_p4(0)& dk_p3(0)& dk_p2(0)& dk_p1(0)& dk_p0(0);
	DK_mPROD <= std_logic_vector (dk_product);
end Behavioral;

