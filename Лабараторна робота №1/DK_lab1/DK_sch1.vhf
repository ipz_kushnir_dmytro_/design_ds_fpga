--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : DK_sch1.vhf
-- /___/   /\     Timestamp : 09/14/2016 10:18:28
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family virtex7 -flat -suppress -vhdl D:/Xilinx/DK_lab1/DK_sch1.vhf -w D:/Xilinx/DK_lab1/DK_sch1.sch
--Design Name: DK_sch1
--Device: virtex7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity DK_sch1 is
   port ( dk_a   : in    std_logic; 
          dk_b   : in    std_logic; 
          dk_d   : in    std_logic; 
          dk_e   : in    std_logic; 
          dk_res : out   std_logic);
end DK_sch1;

architecture BEHAVIORAL of DK_sch1 is
   attribute BOX_TYPE   : string ;
   signal dk_a_or_b                : std_logic;
   signal dk_e_and_d               : std_logic;
   signal dk_na                    : std_logic;
   signal dk_na_xor_e              : std_logic;
   signal dk_nb_or_na_and_na_xor_e : std_logic;
   signal dk_n_a_or_b              : std_logic;
   component AND2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of AND2 : component is "BLACK_BOX";
   
   component OR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of OR2 : component is "BLACK_BOX";
   
   component XOR2
      port ( I0 : in    std_logic; 
             I1 : in    std_logic; 
             O  : out   std_logic);
   end component;
   attribute BOX_TYPE of XOR2 : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
begin
   dk_nb_or_na_and : AND2
      port map (I0=>dk_na_xor_e,
                I1=>dk_n_a_or_b,
                O=>dk_nb_or_na_and_na_xor_e);
   
   XLXI_33 : OR2
      port map (I0=>dk_a,
                I1=>dk_b,
                O=>dk_a_or_b);
   
   XLXI_34 : XOR2
      port map (I0=>dk_e,
                I1=>dk_na,
                O=>dk_na_xor_e);
   
   XLXI_35 : AND2
      port map (I0=>dk_d,
                I1=>dk_e,
                O=>dk_e_and_d);
   
   XLXI_49 : OR2
      port map (I0=>dk_e_and_d,
                I1=>dk_nb_or_na_and_na_xor_e,
                O=>dk_res);
   
   XLXI_51 : INV
      port map (I=>dk_a,
                O=>dk_na);
   
   XLXI_52 : INV
      port map (I=>dk_a_or_b,
                O=>dk_n_a_or_b);
   
end BEHAVIORAL;


