-- Vhdl test bench created from schematic D:\Xilinx\DK_lab1\DK_sch1.sch - Tue Sep 13 18:21:54 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY DK_sch1_DK_sch1_sch_tb IS
END DK_sch1_DK_sch1_sch_tb;
ARCHITECTURE behavioral OF DK_sch1_DK_sch1_sch_tb IS 

   COMPONENT DK_sch1
   PORT( dk_d	:	IN	STD_LOGIC; 
          dk_e	:	IN	STD_LOGIC; 
          dk_a	:	IN	STD_LOGIC; 
          dk_b	:	IN	STD_LOGIC; 
          dk_res	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL dk_d	:	STD_LOGIC;
   SIGNAL dk_e	:	STD_LOGIC;
   SIGNAL dk_a	:	STD_LOGIC;
   SIGNAL dk_b	:	STD_LOGIC;
   SIGNAL dk_res	:	STD_LOGIC;

BEGIN

   UUT: DK_sch1 PORT MAP(
		dk_d => dk_d, 
		dk_e => dk_e, 
		dk_a => dk_a, 
		dk_b => dk_b, 
		dk_res => dk_res
   );

-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
