--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:18:01 09/21/2016
-- Design Name:   
-- Module Name:   D:/Xilinx/DK_lab1/tb_for_v.vhd
-- Project Name:  DK_lab1
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: dk_v1
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_for_v IS
END tb_for_v;
 
ARCHITECTURE behavior OF tb_for_v IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT dk_v1
    PORT(
         dk_a : IN  std_logic;
         dk_b : IN  std_logic;
         dk_d : IN  std_logic;
         dk_e : IN  std_logic;
         dk_res : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal dk_a : std_logic := '0';
   signal dk_b : std_logic := '0';
   signal dk_d : std_logic := '0';
   signal dk_e : std_logic := '0';

 	--Outputs
   signal dk_res : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 

 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: dk_v1 PORT MAP (
          dk_a => dk_a,
          dk_b => dk_b,
          dk_d => dk_d,
          dk_e => dk_e,
          dk_res => dk_res
        );
dk_a<=not dk_a after 10 ns;
dk_b<=not dk_b after 20 ns;
dk_d<=not dk_d after 40 ns;
dk_e<=not dk_e after 80 ns;
  --
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
