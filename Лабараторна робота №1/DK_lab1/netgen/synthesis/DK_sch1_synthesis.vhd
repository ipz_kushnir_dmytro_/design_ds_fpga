--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: DK_sch1_synthesis.vhd
-- /___/   /\     Timestamp: Tue Sep 13 19:19:29 2016
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm DK_sch1 -w -dir netgen/synthesis -ofmt vhdl -sim DK_sch1.ngc DK_sch1_synthesis.vhd 
-- Device	: xc7vx330t-3-ffg1157
-- Input file	: DK_sch1.ngc
-- Output file	: D:\Xilinx\DK_lab1\netgen\synthesis\DK_sch1_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: DK_sch1
-- Xilinx	: D:\Xilinx\14.7\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity DK_sch1 is
  port (
    dk_a : in STD_LOGIC := 'X'; 
    dk_b : in STD_LOGIC := 'X'; 
    dk_d : in STD_LOGIC := 'X'; 
    dk_e : in STD_LOGIC := 'X'; 
    F : out STD_LOGIC 
  );
end DK_sch1;

architecture Structure of DK_sch1 is
  signal dk_a_IBUF_0 : STD_LOGIC; 
  signal dk_b_IBUF_1 : STD_LOGIC; 
  signal dk_d_IBUF_2 : STD_LOGIC; 
  signal dk_e_IBUF_3 : STD_LOGIC; 
  signal dk_na : STD_LOGIC; 
  signal dk_nb : STD_LOGIC; 
  signal dk_e_and_d : STD_LOGIC; 
  signal dk_na_xor_e : STD_LOGIC; 
  signal dk_nb_or_na : STD_LOGIC; 
  signal F_OBUF_9 : STD_LOGIC; 
  signal dk_nb_or_na_and_na_xor_e : STD_LOGIC; 
begin
  XLXI_21 : INV
    port map (
      I => dk_a_IBUF_0,
      O => dk_na
    );
  XLXI_22 : INV
    port map (
      I => dk_b_IBUF_1,
      O => dk_nb
    );
  XLXI_35 : AND2
    port map (
      I0 => dk_d_IBUF_2,
      I1 => dk_e_IBUF_3,
      O => dk_e_and_d
    );
  XLXI_34 : XOR2
    port map (
      I0 => dk_e_IBUF_3,
      I1 => dk_na,
      O => dk_na_xor_e
    );
  XLXI_33 : OR2
    port map (
      I0 => dk_na,
      I1 => dk_nb,
      O => dk_nb_or_na
    );
  XLXI_49 : OR2
    port map (
      I0 => dk_e_and_d,
      I1 => dk_nb_or_na_and_na_xor_e,
      O => F_OBUF_9
    );
  dk_nb_or_na_and : AND2
    port map (
      I0 => dk_na_xor_e,
      I1 => dk_nb_or_na,
      O => dk_nb_or_na_and_na_xor_e
    );
  dk_a_IBUF : IBUF
    port map (
      I => dk_a,
      O => dk_a_IBUF_0
    );
  dk_b_IBUF : IBUF
    port map (
      I => dk_b,
      O => dk_b_IBUF_1
    );
  dk_d_IBUF : IBUF
    port map (
      I => dk_d,
      O => dk_d_IBUF_2
    );
  dk_e_IBUF : IBUF
    port map (
      I => dk_e,
      O => dk_e_IBUF_3
    );
  F_OBUF : OBUF
    port map (
      I => F_OBUF_9,
      O => F
    );

end Structure;

