----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:17:10 09/13/2016 
-- Design Name: 
-- Module Name:    dk_v1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dk_v1 is
    Port ( dk_a : in  STD_LOGIC;
           dk_b : in  STD_LOGIC;
           dk_d : in  STD_LOGIC;
           dk_e : in  STD_LOGIC;
           dk_res : out  STD_LOGIC);
end dk_v1;

architecture Behavioral of dk_v1 is

begin

dk_res <= (not(dk_a or dk_b) and (dk_e xor not dk_a)) or (dk_d and dk_e);
end Behavioral;

