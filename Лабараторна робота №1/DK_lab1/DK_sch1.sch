<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="dk_na_xor_e" />
        <signal name="dk_d" />
        <signal name="dk_e" />
        <signal name="dk_e_and_d" />
        <signal name="dk_nb_or_na_and_na_xor_e" />
        <signal name="dk_res" />
        <signal name="dk_a" />
        <signal name="dk_b" />
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="dk_na" />
        <signal name="XLXN_6" />
        <signal name="dk_a_or_b" />
        <signal name="dk_n_a_or_b" />
        <port polarity="Input" name="dk_d" />
        <port polarity="Input" name="dk_e" />
        <port polarity="Output" name="dk_res" />
        <port polarity="Input" name="dk_a" />
        <port polarity="Input" name="dk_b" />
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="or2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="192" ey="-96" sx="112" sy="-48" r="88" cx="116" cy="-136" />
            <arc ex="48" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <line x2="48" y1="-144" y2="-144" x1="112" />
            <arc ex="112" ey="-144" sx="192" sy="-96" r="88" cx="116" cy="-56" />
            <line x2="48" y1="-48" y2="-48" x1="112" />
        </blockdef>
        <blockdef name="xor2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="60" y1="-128" y2="-128" x1="0" />
            <line x2="208" y1="-96" y2="-96" x1="256" />
            <arc ex="44" ey="-144" sx="48" sy="-48" r="56" cx="16" cy="-96" />
            <arc ex="64" ey="-144" sx="64" sy="-48" r="56" cx="32" cy="-96" />
            <line x2="64" y1="-144" y2="-144" x1="128" />
            <line x2="64" y1="-48" y2="-48" x1="128" />
            <arc ex="128" ey="-144" sx="208" sy="-96" r="88" cx="132" cy="-56" />
            <arc ex="208" ey="-96" sx="128" sy="-48" r="88" cx="132" cy="-136" />
        </blockdef>
        <blockdef name="and2">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="192" y1="-96" y2="-96" x1="256" />
            <arc ex="144" ey="-144" sx="144" sy="-48" r="48" cx="144" cy="-96" />
            <line x2="64" y1="-48" y2="-48" x1="144" />
            <line x2="144" y1="-144" y2="-144" x1="64" />
            <line x2="64" y1="-48" y2="-144" x1="64" />
        </blockdef>
        <block symbolname="and2" name="dk_nb_or_na_and">
            <blockpin signalname="dk_na_xor_e" name="I0" />
            <blockpin signalname="dk_n_a_or_b" name="I1" />
            <blockpin signalname="dk_nb_or_na_and_na_xor_e" name="O" />
        </block>
        <block symbolname="and2" name="XLXI_35">
            <blockpin signalname="dk_d" name="I0" />
            <blockpin signalname="dk_e" name="I1" />
            <blockpin signalname="dk_e_and_d" name="O" />
        </block>
        <block symbolname="xor2" name="XLXI_34">
            <blockpin signalname="dk_e" name="I0" />
            <blockpin signalname="dk_na" name="I1" />
            <blockpin signalname="dk_na_xor_e" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_33">
            <blockpin signalname="dk_a" name="I0" />
            <blockpin signalname="dk_b" name="I1" />
            <blockpin signalname="dk_a_or_b" name="O" />
        </block>
        <block symbolname="or2" name="XLXI_49">
            <blockpin signalname="dk_e_and_d" name="I0" />
            <blockpin signalname="dk_nb_or_na_and_na_xor_e" name="I1" />
            <blockpin signalname="dk_res" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_51">
            <blockpin signalname="dk_a" name="I" />
            <blockpin signalname="dk_na" name="O" />
        </block>
        <block symbolname="inv" name="XLXI_52">
            <blockpin signalname="dk_a_or_b" name="I" />
            <blockpin signalname="dk_n_a_or_b" name="O" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1552" y="1984" name="XLXI_35" orien="R0" />
        <instance x="1552" y="1840" name="XLXI_34" orien="R0" />
        <branch name="dk_d">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1152" y="1920" type="branch" />
            <wire x2="1152" y1="1920" y2="1920" x1="864" />
            <wire x2="1552" y1="1920" y2="1920" x1="1152" />
        </branch>
        <branch name="dk_e">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1168" y="1776" type="branch" />
            <wire x2="1168" y1="1776" y2="1776" x1="864" />
            <wire x2="1552" y1="1776" y2="1776" x1="1168" />
            <wire x2="1168" y1="1776" y2="1856" x1="1168" />
            <wire x2="1552" y1="1856" y2="1856" x1="1168" />
        </branch>
        <iomarker fontsize="28" x="864" y="1776" name="dk_e" orien="R180" />
        <iomarker fontsize="28" x="864" y="1920" name="dk_d" orien="R180" />
        <branch name="dk_nb_or_na_and_na_xor_e">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2368" y="1664" type="branch" />
            <wire x2="2368" y1="1664" y2="1664" x1="2240" />
            <wire x2="2368" y1="1664" y2="1744" x1="2368" />
            <wire x2="2400" y1="1744" y2="1744" x1="2368" />
        </branch>
        <instance x="2400" y="1872" name="XLXI_49" orien="R0" />
        <branch name="dk_e_and_d">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2144" y="1808" type="branch" />
            <wire x2="1824" y1="1888" y2="1888" x1="1808" />
            <wire x2="1824" y1="1808" y2="1888" x1="1824" />
            <wire x2="2144" y1="1808" y2="1808" x1="1824" />
            <wire x2="2400" y1="1808" y2="1808" x1="2144" />
        </branch>
        <branch name="dk_res">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2688" y="1776" type="branch" />
            <wire x2="2688" y1="1776" y2="1776" x1="2656" />
            <wire x2="2768" y1="1776" y2="1776" x1="2688" />
        </branch>
        <branch name="dk_na_xor_e">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1936" y="1696" type="branch" />
            <wire x2="1824" y1="1744" y2="1744" x1="1808" />
            <wire x2="1824" y1="1696" y2="1744" x1="1824" />
            <wire x2="1936" y1="1696" y2="1696" x1="1824" />
            <wire x2="1984" y1="1696" y2="1696" x1="1936" />
        </branch>
        <instance x="1984" y="1760" name="dk_nb_or_na_and" orien="R0" />
        <iomarker fontsize="28" x="2768" y="1776" name="dk_res" orien="R0" />
        <branch name="dk_a">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="976" y="1648" type="branch" />
            <wire x2="976" y1="1648" y2="1648" x1="880" />
            <wire x2="1024" y1="1648" y2="1648" x1="976" />
            <wire x2="1040" y1="1648" y2="1648" x1="1024" />
            <wire x2="1392" y1="1600" y2="1600" x1="1024" />
            <wire x2="1024" y1="1600" y2="1648" x1="1024" />
        </branch>
        <branch name="dk_b">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1008" y="1536" type="branch" />
            <wire x2="1008" y1="1536" y2="1536" x1="896" />
            <wire x2="1392" y1="1536" y2="1536" x1="1008" />
        </branch>
        <iomarker fontsize="28" x="896" y="1536" name="dk_b" orien="R180" />
        <iomarker fontsize="28" x="880" y="1648" name="dk_a" orien="R180" />
        <instance x="1040" y="1680" name="XLXI_51" orien="R0" />
        <branch name="dk_na">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1377" y="1648" type="branch" />
            <wire x2="1377" y1="1648" y2="1648" x1="1264" />
            <wire x2="1408" y1="1648" y2="1648" x1="1377" />
            <wire x2="1408" y1="1648" y2="1712" x1="1408" />
            <wire x2="1552" y1="1712" y2="1712" x1="1408" />
        </branch>
        <instance x="1392" y="1664" name="XLXI_33" orien="R0" />
        <branch name="dk_a_or_b">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1696" y="1568" type="branch" />
            <wire x2="1696" y1="1568" y2="1568" x1="1648" />
            <wire x2="1712" y1="1568" y2="1568" x1="1696" />
        </branch>
        <branch name="dk_n_a_or_b">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="1953" y="1568" type="branch" />
            <wire x2="1953" y1="1568" y2="1568" x1="1936" />
            <wire x2="1968" y1="1568" y2="1568" x1="1953" />
            <wire x2="1968" y1="1568" y2="1632" x1="1968" />
            <wire x2="1984" y1="1632" y2="1632" x1="1968" />
        </branch>
        <instance x="1712" y="1600" name="XLXI_52" orien="R0" />
    </sheet>
</drawing>