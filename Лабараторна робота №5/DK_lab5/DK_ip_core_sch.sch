<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="CLK" />
        <signal name="DK_A(7:0)" />
        <signal name="XLXN_4(7:0)" />
        <signal name="DK_OUT_not_ip(15:0)" />
        <signal name="XLXN_8(7:0)" />
        <signal name="DK_OUT_IP(17:0)" />
        <signal name="rfd" />
        <signal name="rdy" />
        <port polarity="Input" name="CLK" />
        <port polarity="Input" name="DK_A(7:0)" />
        <port polarity="Output" name="DK_OUT_not_ip(15:0)" />
        <port polarity="Output" name="DK_OUT_IP(17:0)" />
        <port polarity="Output" name="rfd" />
        <port polarity="Output" name="rdy" />
        <blockdef name="DK_not_ip_core_sch">
            <timestamp>2016-11-21T9:41:53</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="DK_ip_core_filter">
            <timestamp>2016-11-21T13:48:12</timestamp>
            <rect width="512" x="32" y="32" height="180" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="544" y1="128" y2="128" x1="592" />
            <line x2="548" y1="160" y2="160" x1="592" />
            <line x2="32" y1="144" y2="144" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="592" />
        </blockdef>
        <block symbolname="DK_ip_core_filter" name="XLXI_2">
            <blockpin signalname="DK_A(7:0)" name="din(7:0)" />
            <blockpin signalname="rfd" name="rfd" />
            <blockpin signalname="rdy" name="rdy" />
            <blockpin signalname="CLK" name="clk" />
            <blockpin signalname="DK_OUT_IP(17:0)" name="dout(17:0)" />
        </block>
        <block symbolname="DK_not_ip_core_sch" name="XLXI_1">
            <blockpin signalname="DK_A(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="CLK" name="CLK" />
            <blockpin signalname="DK_OUT_not_ip(15:0)" name="DK_OUT(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="6400" height="6400">
        <attr value="CM" name="LengthUnitName" />
        <attr value="4" name="GridsPerUnit" />
        <iomarker fontsize="28" x="2416" y="464" name="CLK" orien="R180" />
        <instance x="2768" y="800" name="XLXI_1" orien="R0">
        </instance>
        <branch name="CLK">
            <wire x2="2512" y1="464" y2="464" x1="2416" />
            <wire x2="2512" y1="464" y2="512" x1="2512" />
            <wire x2="2704" y1="512" y2="512" x1="2512" />
            <wire x2="2512" y1="512" y2="768" x1="2512" />
            <wire x2="2768" y1="768" y2="768" x1="2512" />
            <wire x2="2704" y1="400" y2="512" x1="2704" />
            <wire x2="2912" y1="400" y2="400" x1="2704" />
        </branch>
        <branch name="DK_A(7:0)">
            <wire x2="2688" y1="400" y2="400" x1="2608" />
            <wire x2="2688" y1="400" y2="704" x1="2688" />
            <wire x2="2768" y1="704" y2="704" x1="2688" />
            <wire x2="2912" y1="336" y2="336" x1="2688" />
            <wire x2="2688" y1="336" y2="400" x1="2688" />
        </branch>
        <iomarker fontsize="28" x="2608" y="400" name="DK_A(7:0)" orien="R180" />
        <branch name="DK_OUT_not_ip(15:0)">
            <wire x2="3184" y1="704" y2="704" x1="3152" />
        </branch>
        <iomarker fontsize="28" x="3184" y="704" name="DK_OUT_not_ip(15:0)" orien="R0" />
        <instance x="2912" y="256" name="XLXI_2" orien="R0">
        </instance>
        <branch name="DK_OUT_IP(17:0)">
            <wire x2="3536" y1="336" y2="336" x1="3504" />
        </branch>
        <iomarker fontsize="28" x="3536" y="336" name="DK_OUT_IP(17:0)" orien="R0" />
        <branch name="rfd">
            <wire x2="3536" y1="384" y2="384" x1="3504" />
        </branch>
        <iomarker fontsize="28" x="3536" y="384" name="rfd" orien="R0" />
        <branch name="rdy">
            <wire x2="3536" y1="416" y2="416" x1="3504" />
        </branch>
        <iomarker fontsize="28" x="3536" y="416" name="rdy" orien="R0" />
    </sheet>
</drawing>