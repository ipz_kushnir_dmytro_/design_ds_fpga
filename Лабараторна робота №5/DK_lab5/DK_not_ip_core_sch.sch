<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="XLXN_1" />
        <signal name="XLXN_2" />
        <signal name="XLXN_3" />
        <signal name="XLXN_4" />
        <signal name="XLXN_5" />
        <signal name="XLXN_6" />
        <signal name="XLXN_7" />
        <signal name="XLXN_8" />
        <signal name="DK_A(7:0)" />
        <signal name="XLXN_15(7:0)" />
        <signal name="XLXN_16(7:0)" />
        <signal name="XLXN_18(7:0)" />
        <signal name="XLXN_9" />
        <signal name="XLXN_11" />
        <signal name="XLXN_31(7:0)" />
        <signal name="MUL1_OUT(15:0)" />
        <signal name="MUL2_OUT(15:0)" />
        <signal name="XLXN_41(7:0)" />
        <signal name="ADD1_OUT(15:0)" />
        <signal name="XLXN_44" />
        <signal name="XLXN_45" />
        <signal name="XLXN_47" />
        <signal name="ADD2_OUT(15:0)" />
        <signal name="ADD3_OUT(15:0)" />
        <signal name="ADD4_OUT(15:0)" />
        <signal name="ADD5_OUT(15:0)" />
        <signal name="XLXN_54" />
        <signal name="XLXN_55" />
        <signal name="MUL3_OUT(15:0)" />
        <signal name="MUL4_OUT(15:0)" />
        <signal name="MUL5_OUT(15:0)" />
        <signal name="MUL6_OUT(15:0)" />
        <signal name="DK_OUT(15:0)" />
        <signal name="XLXN_62" />
        <signal name="XLXN_63" />
        <signal name="CLK" />
        <port polarity="Input" name="DK_A(7:0)" />
        <port polarity="Output" name="DK_OUT(15:0)" />
        <port polarity="Input" name="CLK" />
        <blockdef name="fd8ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="add16">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="384" y1="-128" y2="-128" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="448" />
            <line x2="384" y1="-64" y2="-64" x1="240" />
            <line x2="240" y1="-124" y2="-64" x1="240" />
            <rect width="64" x="0" y="-204" height="24" />
            <rect width="64" x="0" y="-332" height="24" />
            <line x2="64" y1="-320" y2="-320" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="384" y1="-256" y2="-256" x1="448" />
            <rect width="64" x="384" y="-268" height="24" />
            <line x2="64" y1="-448" y2="-448" x1="0" />
            <line x2="64" y1="-448" y2="-448" x1="128" />
            <line x2="128" y1="-416" y2="-448" x1="128" />
            <line x2="64" y1="-288" y2="-432" x1="64" />
            <line x2="64" y1="-256" y2="-288" x1="128" />
            <line x2="128" y1="-224" y2="-256" x1="64" />
            <line x2="64" y1="-80" y2="-224" x1="64" />
            <line x2="64" y1="-160" y2="-80" x1="384" />
            <line x2="384" y1="-336" y2="-160" x1="384" />
            <line x2="384" y1="-352" y2="-336" x1="384" />
            <line x2="384" y1="-432" y2="-352" x1="64" />
            <line x2="336" y1="-128" y2="-148" x1="336" />
            <line x2="336" y1="-128" y2="-128" x1="384" />
        </blockdef>
        <blockdef name="fd16ce">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-128" y2="-128" x1="0" />
            <line x2="64" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="64" y1="-256" y2="-256" x1="0" />
            <line x2="320" y1="-256" y2="-256" x1="384" />
            <line x2="64" y1="-128" y2="-144" x1="80" />
            <line x2="80" y1="-112" y2="-128" x1="64" />
            <rect width="64" x="320" y="-268" height="24" />
            <rect width="64" x="0" y="-268" height="24" />
            <line x2="64" y1="-32" y2="-32" x1="192" />
            <line x2="192" y1="-64" y2="-32" x1="192" />
            <rect width="256" x="64" y="-320" height="256" />
        </blockdef>
        <blockdef name="MUL_104">
            <timestamp>2016-11-21T8:26:53</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="MUL_84">
            <timestamp>2016-11-21T8:27:1</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="vcc">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-64" x1="64" />
            <line x2="64" y1="0" y2="-32" x1="64" />
            <line x2="32" y1="-64" y2="-64" x1="96" />
        </blockdef>
        <block symbolname="fd8ce" name="XLXI_1">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_5" name="CE" />
            <blockpin signalname="XLXN_4" name="CLR" />
            <blockpin signalname="DK_A(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_15(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_2">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_6" name="CE" />
            <blockpin signalname="XLXN_3" name="CLR" />
            <blockpin signalname="XLXN_15(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_16(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_3">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_8" name="CE" />
            <blockpin signalname="XLXN_1" name="CLR" />
            <blockpin signalname="XLXN_16(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_18(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="fd8ce" name="XLXI_4">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_7" name="CE" />
            <blockpin signalname="XLXN_2" name="CLR" />
            <blockpin signalname="XLXN_18(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_41(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="add16" name="XLXI_6">
            <blockpin signalname="MUL1_OUT(15:0)" name="A(15:0)" />
            <blockpin signalname="MUL2_OUT(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_47" name="CI" />
            <blockpin signalname="XLXN_44" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="ADD1_OUT(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_7">
            <blockpin signalname="MUL3_OUT(15:0)" name="A(15:0)" />
            <blockpin signalname="ADD1_OUT(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_44" name="CI" />
            <blockpin signalname="XLXN_45" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="ADD2_OUT(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_8">
            <blockpin signalname="MUL4_OUT(15:0)" name="A(15:0)" />
            <blockpin signalname="ADD2_OUT(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_45" name="CI" />
            <blockpin signalname="XLXN_55" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="ADD3_OUT(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_9">
            <blockpin signalname="MUL5_OUT(15:0)" name="A(15:0)" />
            <blockpin signalname="ADD3_OUT(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_55" name="CI" />
            <blockpin signalname="XLXN_54" name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="ADD4_OUT(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="add16" name="XLXI_10">
            <blockpin signalname="MUL6_OUT(15:0)" name="A(15:0)" />
            <blockpin signalname="ADD4_OUT(15:0)" name="B(15:0)" />
            <blockpin signalname="XLXN_54" name="CI" />
            <blockpin name="CO" />
            <blockpin name="OFL" />
            <blockpin signalname="ADD5_OUT(15:0)" name="S(15:0)" />
        </block>
        <block symbolname="fd16ce" name="XLXI_13">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_62" name="CE" />
            <blockpin signalname="XLXN_63" name="CLR" />
            <blockpin signalname="ADD5_OUT(15:0)" name="D(15:0)" />
            <blockpin signalname="DK_OUT(15:0)" name="Q(15:0)" />
        </block>
        <block symbolname="MUL_104" name="XLXI_14">
            <blockpin signalname="DK_A(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="MUL1_OUT(15:0)" name="DK_Opt(15:0)" />
        </block>
        <block symbolname="MUL_104" name="XLXI_15">
            <blockpin signalname="XLXN_15(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="MUL2_OUT(15:0)" name="DK_Opt(15:0)" />
        </block>
        <block symbolname="MUL_84" name="XLXI_16">
            <blockpin signalname="XLXN_16(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="MUL3_OUT(15:0)" name="DK_Opt(15:0)" />
        </block>
        <block symbolname="MUL_84" name="XLXI_17">
            <blockpin signalname="XLXN_18(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="MUL4_OUT(15:0)" name="DK_Opt(15:0)" />
        </block>
        <block symbolname="MUL_84" name="XLXI_18">
            <blockpin signalname="XLXN_41(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="MUL5_OUT(15:0)" name="DK_Opt(15:0)" />
        </block>
        <block symbolname="MUL_104" name="XLXI_19">
            <blockpin signalname="XLXN_31(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="MUL6_OUT(15:0)" name="DK_Opt(15:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_20">
            <blockpin signalname="XLXN_4" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_21">
            <blockpin signalname="XLXN_3" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_22">
            <blockpin signalname="XLXN_1" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_23">
            <blockpin signalname="XLXN_2" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_24">
            <blockpin signalname="XLXN_5" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_25">
            <blockpin signalname="XLXN_6" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_26">
            <blockpin signalname="XLXN_8" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_27">
            <blockpin signalname="XLXN_7" name="P" />
        </block>
        <block symbolname="vcc" name="XLXI_28">
            <blockpin signalname="XLXN_9" name="P" />
        </block>
        <block symbolname="fd8ce" name="XLXI_5">
            <blockpin signalname="CLK" name="C" />
            <blockpin signalname="XLXN_9" name="CE" />
            <blockpin signalname="XLXN_11" name="CLR" />
            <blockpin signalname="XLXN_41(7:0)" name="D(7:0)" />
            <blockpin signalname="XLXN_31(7:0)" name="Q(7:0)" />
        </block>
        <block symbolname="gnd" name="XLXI_30">
            <blockpin signalname="XLXN_11" name="G" />
        </block>
        <block symbolname="gnd" name="XLXI_44">
            <blockpin signalname="XLXN_47" name="G" />
        </block>
        <block symbolname="vcc" name="XLXI_45">
            <blockpin signalname="XLXN_62" name="P" />
        </block>
        <block symbolname="gnd" name="XLXI_46">
            <blockpin signalname="XLXN_63" name="G" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="6400" height="6400">
        <attr value="CM" name="LengthUnitName" />
        <attr value="4" name="GridsPerUnit" />
        <instance x="1776" y="2160" name="XLXI_2" orien="R0" />
        <instance x="1120" y="2160" name="XLXI_1" orien="R0" />
        <instance x="1024" y="3328" name="XLXI_6" orien="R0" />
        <instance x="4096" y="3312" name="XLXI_10" orien="R0" />
        <instance x="3248" y="3312" name="XLXI_9" orien="R0" />
        <instance x="2528" y="3312" name="XLXI_8" orien="R0" />
        <instance x="1776" y="3344" name="XLXI_7" orien="R0" />
        <instance x="1088" y="2576" name="XLXI_14" orien="R0">
        </instance>
        <instance x="1728" y="2576" name="XLXI_15" orien="R0">
        </instance>
        <branch name="XLXN_2">
            <wire x2="3088" y1="2128" y2="2144" x1="3088" />
            <wire x2="3120" y1="2128" y2="2128" x1="3088" />
        </branch>
        <branch name="XLXN_3">
            <wire x2="1776" y1="2128" y2="2160" x1="1776" />
        </branch>
        <instance x="1712" y="2288" name="XLXI_21" orien="R0" />
        <branch name="XLXN_4">
            <wire x2="1120" y1="2128" y2="2144" x1="1120" />
            <wire x2="1120" y1="2144" y2="2160" x1="1120" />
        </branch>
        <instance x="1056" y="2288" name="XLXI_20" orien="R0" />
        <branch name="XLXN_5">
            <wire x2="1120" y1="1968" y2="1968" x1="1088" />
        </branch>
        <instance x="1088" y="1904" name="XLXI_24" orien="M90" />
        <instance x="1760" y="1904" name="XLXI_25" orien="M90" />
        <branch name="XLXN_6">
            <wire x2="1776" y1="1968" y2="1968" x1="1760" />
        </branch>
        <branch name="XLXN_8">
            <wire x2="2464" y1="1968" y2="1968" x1="2400" />
        </branch>
        <branch name="DK_A(7:0)">
            <wire x2="960" y1="1904" y2="1904" x1="848" />
            <wire x2="960" y1="1904" y2="2544" x1="960" />
            <wire x2="1088" y1="2544" y2="2544" x1="960" />
            <wire x2="1120" y1="1904" y2="1904" x1="960" />
        </branch>
        <iomarker fontsize="28" x="848" y="1904" name="DK_A(7:0)" orien="R180" />
        <branch name="XLXN_15(7:0)">
            <wire x2="1616" y1="1904" y2="1904" x1="1504" />
            <wire x2="1776" y1="1904" y2="1904" x1="1616" />
            <wire x2="1616" y1="1904" y2="2544" x1="1616" />
            <wire x2="1728" y1="2544" y2="2544" x1="1616" />
        </branch>
        <branch name="XLXN_16(7:0)">
            <wire x2="2256" y1="1904" y2="1904" x1="2160" />
            <wire x2="2464" y1="1904" y2="1904" x1="2256" />
            <wire x2="2256" y1="1904" y2="2480" x1="2256" />
            <wire x2="2512" y1="2480" y2="2480" x1="2256" />
            <wire x2="2512" y1="2480" y2="2528" x1="2512" />
        </branch>
        <branch name="XLXN_18(7:0)">
            <wire x2="2944" y1="1904" y2="1904" x1="2848" />
            <wire x2="3120" y1="1904" y2="1904" x1="2944" />
            <wire x2="2944" y1="1904" y2="2464" x1="2944" />
            <wire x2="3152" y1="2464" y2="2464" x1="2944" />
            <wire x2="3152" y1="2464" y2="2512" x1="3152" />
        </branch>
        <branch name="XLXN_7">
            <wire x2="3120" y1="1968" y2="1968" x1="3072" />
        </branch>
        <branch name="XLXN_9">
            <wire x2="3936" y1="1968" y2="1968" x1="3856" />
        </branch>
        <branch name="MUL1_OUT(15:0)">
            <wire x2="1552" y1="2672" y2="2672" x1="944" />
            <wire x2="944" y1="2672" y2="3008" x1="944" />
            <wire x2="1024" y1="3008" y2="3008" x1="944" />
            <wire x2="1552" y1="2544" y2="2544" x1="1472" />
            <wire x2="1552" y1="2544" y2="2672" x1="1552" />
        </branch>
        <branch name="MUL2_OUT(15:0)">
            <wire x2="832" y1="2752" y2="3136" x1="832" />
            <wire x2="1024" y1="3136" y2="3136" x1="832" />
            <wire x2="2384" y1="2752" y2="2752" x1="832" />
            <wire x2="2384" y1="2544" y2="2544" x1="2112" />
            <wire x2="2384" y1="2544" y2="2752" x1="2384" />
        </branch>
        <instance x="2512" y="2560" name="XLXI_16" orien="R0">
        </instance>
        <instance x="3152" y="2544" name="XLXI_17" orien="R0">
        </instance>
        <instance x="3872" y="2544" name="XLXI_18" orien="R0">
        </instance>
        <instance x="2464" y="2160" name="XLXI_3" orien="R0" />
        <branch name="XLXN_1">
            <wire x2="2464" y1="2128" y2="2144" x1="2464" />
        </branch>
        <instance x="2400" y="1904" name="XLXI_26" orien="M90" />
        <instance x="2400" y="2272" name="XLXI_22" orien="R0" />
        <instance x="3120" y="2160" name="XLXI_4" orien="R0" />
        <instance x="3072" y="1904" name="XLXI_27" orien="M90" />
        <instance x="3024" y="2272" name="XLXI_23" orien="R0" />
        <instance x="3936" y="2160" name="XLXI_5" orien="R0" />
        <branch name="XLXN_31(7:0)">
            <wire x2="4384" y1="1904" y2="1904" x1="4320" />
            <wire x2="4400" y1="1904" y2="1904" x1="4384" />
            <wire x2="4400" y1="1904" y2="2512" x1="4400" />
            <wire x2="4464" y1="2512" y2="2512" x1="4400" />
        </branch>
        <instance x="3856" y="1904" name="XLXI_28" orien="M90" />
        <branch name="XLXN_11">
            <wire x2="3936" y1="2144" y2="2144" x1="3904" />
            <wire x2="3904" y1="2144" y2="2176" x1="3904" />
            <wire x2="3936" y1="2128" y2="2144" x1="3936" />
        </branch>
        <instance x="3840" y="2304" name="XLXI_30" orien="R0" />
        <branch name="XLXN_41(7:0)">
            <wire x2="3696" y1="1904" y2="1904" x1="3504" />
            <wire x2="3936" y1="1904" y2="1904" x1="3696" />
            <wire x2="3696" y1="1904" y2="2512" x1="3696" />
            <wire x2="3872" y1="2512" y2="2512" x1="3696" />
        </branch>
        <instance x="4464" y="2544" name="XLXI_19" orien="R0">
        </instance>
        <instance x="4656" y="3728" name="XLXI_13" orien="R0" />
        <branch name="ADD1_OUT(15:0)">
            <wire x2="1616" y1="3072" y2="3072" x1="1472" />
            <wire x2="1616" y1="3072" y2="3152" x1="1616" />
            <wire x2="1776" y1="3152" y2="3152" x1="1616" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="1600" y1="3264" y2="3264" x1="1472" />
            <wire x2="1600" y1="2896" y2="3264" x1="1600" />
            <wire x2="1776" y1="2896" y2="2896" x1="1600" />
        </branch>
        <branch name="XLXN_45">
            <wire x2="2368" y1="3280" y2="3280" x1="2224" />
            <wire x2="2368" y1="2864" y2="3280" x1="2368" />
            <wire x2="2528" y1="2864" y2="2864" x1="2368" />
        </branch>
        <instance x="848" y="3008" name="XLXI_44" orien="R0" />
        <branch name="XLXN_47">
            <wire x2="912" y1="2864" y2="2880" x1="912" />
            <wire x2="1024" y1="2864" y2="2864" x1="912" />
            <wire x2="1024" y1="2864" y2="2880" x1="1024" />
        </branch>
        <branch name="ADD2_OUT(15:0)">
            <wire x2="2352" y1="3088" y2="3088" x1="2224" />
            <wire x2="2352" y1="3088" y2="3120" x1="2352" />
            <wire x2="2528" y1="3120" y2="3120" x1="2352" />
        </branch>
        <branch name="ADD3_OUT(15:0)">
            <wire x2="3104" y1="3056" y2="3056" x1="2976" />
            <wire x2="3104" y1="3056" y2="3120" x1="3104" />
            <wire x2="3248" y1="3120" y2="3120" x1="3104" />
        </branch>
        <branch name="ADD4_OUT(15:0)">
            <wire x2="3888" y1="3056" y2="3056" x1="3696" />
            <wire x2="3888" y1="3056" y2="3120" x1="3888" />
            <wire x2="4096" y1="3120" y2="3120" x1="3888" />
        </branch>
        <branch name="ADD5_OUT(15:0)">
            <wire x2="4592" y1="3056" y2="3056" x1="4544" />
            <wire x2="4592" y1="3056" y2="3472" x1="4592" />
            <wire x2="4656" y1="3472" y2="3472" x1="4592" />
        </branch>
        <branch name="XLXN_54">
            <wire x2="3872" y1="3248" y2="3248" x1="3696" />
            <wire x2="3872" y1="2864" y2="3248" x1="3872" />
            <wire x2="4096" y1="2864" y2="2864" x1="3872" />
        </branch>
        <branch name="XLXN_55">
            <wire x2="3088" y1="3248" y2="3248" x1="2976" />
            <wire x2="3088" y1="2864" y2="3248" x1="3088" />
            <wire x2="3248" y1="2864" y2="2864" x1="3088" />
        </branch>
        <branch name="MUL3_OUT(15:0)">
            <wire x2="1776" y1="3024" y2="3024" x1="1696" />
            <wire x2="1696" y1="3024" y2="3360" x1="1696" />
            <wire x2="3040" y1="3360" y2="3360" x1="1696" />
            <wire x2="3040" y1="2528" y2="2528" x1="2896" />
            <wire x2="3040" y1="2528" y2="3360" x1="3040" />
        </branch>
        <branch name="MUL4_OUT(15:0)">
            <wire x2="2464" y1="2800" y2="2992" x1="2464" />
            <wire x2="2528" y1="2992" y2="2992" x1="2464" />
            <wire x2="3600" y1="2800" y2="2800" x1="2464" />
            <wire x2="3600" y1="2512" y2="2512" x1="3536" />
            <wire x2="3600" y1="2512" y2="2800" x1="3600" />
        </branch>
        <branch name="MUL5_OUT(15:0)">
            <wire x2="3184" y1="2784" y2="2992" x1="3184" />
            <wire x2="3248" y1="2992" y2="2992" x1="3184" />
            <wire x2="4320" y1="2784" y2="2784" x1="3184" />
            <wire x2="4320" y1="2512" y2="2512" x1="4256" />
            <wire x2="4320" y1="2512" y2="2784" x1="4320" />
        </branch>
        <branch name="MUL6_OUT(15:0)">
            <wire x2="4032" y1="2800" y2="2992" x1="4032" />
            <wire x2="4096" y1="2992" y2="2992" x1="4032" />
            <wire x2="4928" y1="2800" y2="2800" x1="4032" />
            <wire x2="4928" y1="2512" y2="2512" x1="4848" />
            <wire x2="4928" y1="2512" y2="2800" x1="4928" />
        </branch>
        <branch name="DK_OUT(15:0)">
            <wire x2="5072" y1="3472" y2="3472" x1="5040" />
        </branch>
        <iomarker fontsize="28" x="5072" y="3472" name="DK_OUT(15:0)" orien="R0" />
        <instance x="4464" y="3488" name="XLXI_45" orien="R0" />
        <branch name="XLXN_62">
            <wire x2="4528" y1="3488" y2="3536" x1="4528" />
            <wire x2="4656" y1="3536" y2="3536" x1="4528" />
        </branch>
        <branch name="XLXN_63">
            <wire x2="4656" y1="3696" y2="3728" x1="4656" />
        </branch>
        <instance x="4592" y="3856" name="XLXI_46" orien="R0" />
        <branch name="CLK">
            <wire x2="880" y1="2016" y2="2016" x1="784" />
            <wire x2="1104" y1="2016" y2="2016" x1="880" />
            <wire x2="1104" y1="2016" y2="2032" x1="1104" />
            <wire x2="1120" y1="2032" y2="2032" x1="1104" />
            <wire x2="880" y1="1776" y2="2016" x1="880" />
            <wire x2="1568" y1="1776" y2="1776" x1="880" />
            <wire x2="1568" y1="1776" y2="2032" x1="1568" />
            <wire x2="1776" y1="2032" y2="2032" x1="1568" />
            <wire x2="2224" y1="1776" y2="1776" x1="1568" />
            <wire x2="2224" y1="1776" y2="2032" x1="2224" />
            <wire x2="2464" y1="2032" y2="2032" x1="2224" />
            <wire x2="2912" y1="1776" y2="1776" x1="2224" />
            <wire x2="2912" y1="1776" y2="2032" x1="2912" />
            <wire x2="3120" y1="2032" y2="2032" x1="2912" />
            <wire x2="3520" y1="1776" y2="1776" x1="2912" />
            <wire x2="3520" y1="1776" y2="2032" x1="3520" />
            <wire x2="3936" y1="2032" y2="2032" x1="3520" />
            <wire x2="3776" y1="1776" y2="1776" x1="3520" />
            <wire x2="3776" y1="1776" y2="3600" x1="3776" />
            <wire x2="4656" y1="3600" y2="3600" x1="3776" />
        </branch>
        <iomarker fontsize="28" x="784" y="2016" name="CLK" orien="R180" />
    </sheet>
</drawing>