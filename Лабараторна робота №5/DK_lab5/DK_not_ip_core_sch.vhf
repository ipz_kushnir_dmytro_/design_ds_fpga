--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : DK_not_ip_core_sch.vhf
-- /___/   /\     Timestamp : 11/21/2016 15:36:50
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -sympath D:/Xilinx/DK_lab5/ipcore_dir -intstyle ise -family virtex7 -flat -suppress -vhdl D:/Xilinx/DK_lab5/DK_not_ip_core_sch.vhf -w D:/Xilinx/DK_lab5/DK_not_ip_core_sch.sch
--Design Name: DK_not_ip_core_sch
--Device: virtex7
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--
----- CELL FD16CE_HXILINX_DK_not_ip_core_sch -----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FD16CE_HXILINX_DK_not_ip_core_sch is
port (
    Q   : out STD_LOGIC_VECTOR(15 downto 0) := (others => '0');

    C   : in STD_LOGIC;
    CE  : in STD_LOGIC;
    CLR : in STD_LOGIC;
    D   : in STD_LOGIC_VECTOR(15 downto 0)
    );
end FD16CE_HXILINX_DK_not_ip_core_sch;

architecture Behavioral of FD16CE_HXILINX_DK_not_ip_core_sch is

begin

process(C, CLR)
begin
  if (CLR='1') then
    Q <= (others => '0');
  elsif (C'event and C = '1') then
    if (CE='1') then 
      Q <= D;
    end if;
  end if;
end process;


end Behavioral;

----- CELL FD8CE_HXILINX_DK_not_ip_core_sch -----


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FD8CE_HXILINX_DK_not_ip_core_sch is
port (
    Q   : out STD_LOGIC_VECTOR(7 downto 0) := (others => '0');

    C   : in STD_LOGIC;
    CE  : in STD_LOGIC;
    CLR : in STD_LOGIC;
    D   : in STD_LOGIC_VECTOR(7 downto 0)
    );
end FD8CE_HXILINX_DK_not_ip_core_sch;

architecture Behavioral of FD8CE_HXILINX_DK_not_ip_core_sch is

begin

process(C, CLR)
begin
  if (CLR='1') then
    Q <= (others => '0');
  elsif (C'event and C = '1') then
    if (CE='1') then 
      Q <= D;
    end if;
  end if;
end process;


end Behavioral;

----- CELL ADD16_HXILINX_DK_not_ip_core_sch -----
  
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity ADD16_HXILINX_DK_not_ip_core_sch is
port(
       CO  : out std_logic;
       OFL : out std_logic;
       S   : out std_logic_vector(15 downto 0);
    
       A   : in std_logic_vector(15 downto 0);
       B   : in std_logic_vector(15 downto 0);
       CI  : in std_logic
    );
end ADD16_HXILINX_DK_not_ip_core_sch;

architecture ADD16_HXILINX_DK_not_ip_core_sch_V of ADD16_HXILINX_DK_not_ip_core_sch is
  signal adder_tmp: std_logic_vector(16 downto 0);
begin
  adder_tmp <= conv_std_logic_vector((conv_integer(A) + conv_integer(B) + conv_integer(CI)),17);
  S         <= adder_tmp(15 downto 0);
  CO        <= adder_tmp(16);
  OFL <=  ( A(15) and B(15) and (not adder_tmp(15)) ) or ( (not A(15)) and (not B(15)) and adder_tmp(15) );  
          
end ADD16_HXILINX_DK_not_ip_core_sch_V;
 

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity DK_not_ip_core_sch is
   port ( CLK    : in    std_logic; 
          DK_A   : in    std_logic_vector (7 downto 0); 
          DK_OUT : out   std_logic_vector (15 downto 0));
end DK_not_ip_core_sch;

architecture BEHAVIORAL of DK_not_ip_core_sch is
   attribute HU_SET     : string ;
   attribute BOX_TYPE   : string ;
   signal ADD1_OUT : std_logic_vector (15 downto 0);
   signal ADD2_OUT : std_logic_vector (15 downto 0);
   signal ADD3_OUT : std_logic_vector (15 downto 0);
   signal ADD4_OUT : std_logic_vector (15 downto 0);
   signal ADD5_OUT : std_logic_vector (15 downto 0);
   signal MUL1_OUT : std_logic_vector (15 downto 0);
   signal MUL2_OUT : std_logic_vector (15 downto 0);
   signal MUL3_OUT : std_logic_vector (15 downto 0);
   signal MUL4_OUT : std_logic_vector (15 downto 0);
   signal MUL5_OUT : std_logic_vector (15 downto 0);
   signal MUL6_OUT : std_logic_vector (15 downto 0);
   signal XLXN_1   : std_logic;
   signal XLXN_2   : std_logic;
   signal XLXN_3   : std_logic;
   signal XLXN_4   : std_logic;
   signal XLXN_5   : std_logic;
   signal XLXN_6   : std_logic;
   signal XLXN_7   : std_logic;
   signal XLXN_8   : std_logic;
   signal XLXN_9   : std_logic;
   signal XLXN_11  : std_logic;
   signal XLXN_15  : std_logic_vector (7 downto 0);
   signal XLXN_16  : std_logic_vector (7 downto 0);
   signal XLXN_18  : std_logic_vector (7 downto 0);
   signal XLXN_31  : std_logic_vector (7 downto 0);
   signal XLXN_41  : std_logic_vector (7 downto 0);
   signal XLXN_44  : std_logic;
   signal XLXN_45  : std_logic;
   signal XLXN_47  : std_logic;
   signal XLXN_54  : std_logic;
   signal XLXN_55  : std_logic;
   signal XLXN_62  : std_logic;
   signal XLXN_63  : std_logic;
   component FD8CE_HXILINX_DK_not_ip_core_sch
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic_vector (7 downto 0); 
             Q   : out   std_logic_vector (7 downto 0));
   end component;
   
   component ADD16_HXILINX_DK_not_ip_core_sch
      port ( A   : in    std_logic_vector (15 downto 0); 
             B   : in    std_logic_vector (15 downto 0); 
             CI  : in    std_logic; 
             CO  : out   std_logic; 
             OFL : out   std_logic; 
             S   : out   std_logic_vector (15 downto 0));
   end component;
   
   component FD16CE_HXILINX_DK_not_ip_core_sch
      port ( C   : in    std_logic; 
             CE  : in    std_logic; 
             CLR : in    std_logic; 
             D   : in    std_logic_vector (15 downto 0); 
             Q   : out   std_logic_vector (15 downto 0));
   end component;
   
   component MUL_104
      port ( DK_A   : in    std_logic_vector (7 downto 0); 
             DK_Opt : out   std_logic_vector (15 downto 0));
   end component;
   
   component MUL_84
      port ( DK_A   : in    std_logic_vector (7 downto 0); 
             DK_Opt : out   std_logic_vector (15 downto 0));
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component VCC
      port ( P : out   std_logic);
   end component;
   attribute BOX_TYPE of VCC : component is "BLACK_BOX";
   
   attribute HU_SET of XLXI_1 : label is "XLXI_1_0";
   attribute HU_SET of XLXI_2 : label is "XLXI_2_1";
   attribute HU_SET of XLXI_3 : label is "XLXI_3_2";
   attribute HU_SET of XLXI_4 : label is "XLXI_4_3";
   attribute HU_SET of XLXI_5 : label is "XLXI_5_10";
   attribute HU_SET of XLXI_6 : label is "XLXI_6_4";
   attribute HU_SET of XLXI_7 : label is "XLXI_7_5";
   attribute HU_SET of XLXI_8 : label is "XLXI_8_6";
   attribute HU_SET of XLXI_9 : label is "XLXI_9_7";
   attribute HU_SET of XLXI_10 : label is "XLXI_10_8";
   attribute HU_SET of XLXI_13 : label is "XLXI_13_9";
begin
   XLXI_1 : FD8CE_HXILINX_DK_not_ip_core_sch
      port map (C=>CLK,
                CE=>XLXN_5,
                CLR=>XLXN_4,
                D(7 downto 0)=>DK_A(7 downto 0),
                Q(7 downto 0)=>XLXN_15(7 downto 0));
   
   XLXI_2 : FD8CE_HXILINX_DK_not_ip_core_sch
      port map (C=>CLK,
                CE=>XLXN_6,
                CLR=>XLXN_3,
                D(7 downto 0)=>XLXN_15(7 downto 0),
                Q(7 downto 0)=>XLXN_16(7 downto 0));
   
   XLXI_3 : FD8CE_HXILINX_DK_not_ip_core_sch
      port map (C=>CLK,
                CE=>XLXN_8,
                CLR=>XLXN_1,
                D(7 downto 0)=>XLXN_16(7 downto 0),
                Q(7 downto 0)=>XLXN_18(7 downto 0));
   
   XLXI_4 : FD8CE_HXILINX_DK_not_ip_core_sch
      port map (C=>CLK,
                CE=>XLXN_7,
                CLR=>XLXN_2,
                D(7 downto 0)=>XLXN_18(7 downto 0),
                Q(7 downto 0)=>XLXN_41(7 downto 0));
   
   XLXI_5 : FD8CE_HXILINX_DK_not_ip_core_sch
      port map (C=>CLK,
                CE=>XLXN_9,
                CLR=>XLXN_11,
                D(7 downto 0)=>XLXN_41(7 downto 0),
                Q(7 downto 0)=>XLXN_31(7 downto 0));
   
   XLXI_6 : ADD16_HXILINX_DK_not_ip_core_sch
      port map (A(15 downto 0)=>MUL1_OUT(15 downto 0),
                B(15 downto 0)=>MUL2_OUT(15 downto 0),
                CI=>XLXN_47,
                CO=>XLXN_44,
                OFL=>open,
                S(15 downto 0)=>ADD1_OUT(15 downto 0));
   
   XLXI_7 : ADD16_HXILINX_DK_not_ip_core_sch
      port map (A(15 downto 0)=>MUL3_OUT(15 downto 0),
                B(15 downto 0)=>ADD1_OUT(15 downto 0),
                CI=>XLXN_44,
                CO=>XLXN_45,
                OFL=>open,
                S(15 downto 0)=>ADD2_OUT(15 downto 0));
   
   XLXI_8 : ADD16_HXILINX_DK_not_ip_core_sch
      port map (A(15 downto 0)=>MUL4_OUT(15 downto 0),
                B(15 downto 0)=>ADD2_OUT(15 downto 0),
                CI=>XLXN_45,
                CO=>XLXN_55,
                OFL=>open,
                S(15 downto 0)=>ADD3_OUT(15 downto 0));
   
   XLXI_9 : ADD16_HXILINX_DK_not_ip_core_sch
      port map (A(15 downto 0)=>MUL5_OUT(15 downto 0),
                B(15 downto 0)=>ADD3_OUT(15 downto 0),
                CI=>XLXN_55,
                CO=>XLXN_54,
                OFL=>open,
                S(15 downto 0)=>ADD4_OUT(15 downto 0));
   
   XLXI_10 : ADD16_HXILINX_DK_not_ip_core_sch
      port map (A(15 downto 0)=>MUL6_OUT(15 downto 0),
                B(15 downto 0)=>ADD4_OUT(15 downto 0),
                CI=>XLXN_54,
                CO=>open,
                OFL=>open,
                S(15 downto 0)=>ADD5_OUT(15 downto 0));
   
   XLXI_13 : FD16CE_HXILINX_DK_not_ip_core_sch
      port map (C=>CLK,
                CE=>XLXN_62,
                CLR=>XLXN_63,
                D(15 downto 0)=>ADD5_OUT(15 downto 0),
                Q(15 downto 0)=>DK_OUT(15 downto 0));
   
   XLXI_14 : MUL_104
      port map (DK_A(7 downto 0)=>DK_A(7 downto 0),
                DK_Opt(15 downto 0)=>MUL1_OUT(15 downto 0));
   
   XLXI_15 : MUL_104
      port map (DK_A(7 downto 0)=>XLXN_15(7 downto 0),
                DK_Opt(15 downto 0)=>MUL2_OUT(15 downto 0));
   
   XLXI_16 : MUL_84
      port map (DK_A(7 downto 0)=>XLXN_16(7 downto 0),
                DK_Opt(15 downto 0)=>MUL3_OUT(15 downto 0));
   
   XLXI_17 : MUL_84
      port map (DK_A(7 downto 0)=>XLXN_18(7 downto 0),
                DK_Opt(15 downto 0)=>MUL4_OUT(15 downto 0));
   
   XLXI_18 : MUL_84
      port map (DK_A(7 downto 0)=>XLXN_41(7 downto 0),
                DK_Opt(15 downto 0)=>MUL5_OUT(15 downto 0));
   
   XLXI_19 : MUL_104
      port map (DK_A(7 downto 0)=>XLXN_31(7 downto 0),
                DK_Opt(15 downto 0)=>MUL6_OUT(15 downto 0));
   
   XLXI_20 : GND
      port map (G=>XLXN_4);
   
   XLXI_21 : GND
      port map (G=>XLXN_3);
   
   XLXI_22 : GND
      port map (G=>XLXN_1);
   
   XLXI_23 : GND
      port map (G=>XLXN_2);
   
   XLXI_24 : VCC
      port map (P=>XLXN_5);
   
   XLXI_25 : VCC
      port map (P=>XLXN_6);
   
   XLXI_26 : VCC
      port map (P=>XLXN_8);
   
   XLXI_27 : VCC
      port map (P=>XLXN_7);
   
   XLXI_28 : VCC
      port map (P=>XLXN_9);
   
   XLXI_30 : GND
      port map (G=>XLXN_11);
   
   XLXI_44 : GND
      port map (G=>XLXN_47);
   
   XLXI_45 : VCC
      port map (P=>XLXN_62);
   
   XLXI_46 : GND
      port map (G=>XLXN_63);
   
end BEHAVIORAL;


