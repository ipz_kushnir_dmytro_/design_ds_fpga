-- Vhdl test bench created from schematic D:\Xilinx\DK_lab5\DK_ip_core_sch.sch - Mon Nov 21 15:50:40 2016
--
-- Notes: 
-- 1) This testbench template has been automatically generated using types
-- std_logic and std_logic_vector for the ports of the unit under test.
-- Xilinx recommends that these types always be used for the top-level
-- I/O of a design in order to guarantee that the testbench will bind
-- correctly to the timing (post-route) simulation model.
-- 2) To use this template as your testbench, change the filename to any
-- name of your choice with the extension .vhd, and use the "Source->Add"
-- menu in Project Navigator to import the testbench. Then
-- edit the user defined section below, adding code to generate the 
-- stimulus for your design.
--
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;
ENTITY DK_ip_core_sch_DK_ip_core_sch_sch_tb IS
END DK_ip_core_sch_DK_ip_core_sch_sch_tb;
ARCHITECTURE behavioral OF DK_ip_core_sch_DK_ip_core_sch_sch_tb IS 

   COMPONENT DK_ip_core_sch
   PORT( CLK	:	IN	STD_LOGIC; 
          DK_A	:	IN	STD_LOGIC_VECTOR (7 DOWNTO 0); 
          DK_OUT_not_ip	:	OUT	STD_LOGIC_VECTOR (15 DOWNTO 0); 
          DK_OUT_IP	:	OUT	STD_LOGIC_VECTOR (17 DOWNTO 0); 
          rfd	:	OUT	STD_LOGIC; 
          rdy	:	OUT	STD_LOGIC);
   END COMPONENT;

   SIGNAL CLK	:	STD_LOGIC;
   SIGNAL DK_A	:	STD_LOGIC_VECTOR (7 DOWNTO 0);
   SIGNAL DK_OUT_not_ip	:	STD_LOGIC_VECTOR (15 DOWNTO 0);
   SIGNAL DK_OUT_IP	:	STD_LOGIC_VECTOR (17 DOWNTO 0);
   SIGNAL rfd	:	STD_LOGIC;
   SIGNAL rdy	:	STD_LOGIC;
constant clk_c : time := 10 ns;
		SIGNAL  buf:  integer := 0;
BEGIN

   UUT: DK_ip_core_sch PORT MAP(
		CLK => CLK, 
		DK_A => DK_A, 
		DK_OUT_not_ip => DK_OUT_not_ip, 
		DK_OUT_IP => DK_OUT_IP, 
		rfd => rfd, 
		rdy => rdy
   );
CLK_process :process
	begin
		buf <= buf + 1;
	DK_A <= std_logic_vector(to_unsigned(buf,DK_A'length));
	CLK <= '0';
	wait for clk_c/2;
	CLK <= '1';
	wait for clk_c/2;
	end process;
-- *** Test Bench - User Defined Section ***
   tb : PROCESS
   BEGIN
      WAIT; -- will wait forever
   END PROCESS;
-- *** End Test Bench - User Defined Section ***

END;
