----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:07:48 09/18/2016 
-- Design Name: 
-- Module Name:    DK_C_mM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MUL_104 is
Port ( DK_A : in STD_LOGIC_VECTOR (7 downto 0);
DK_Opt : out STD_LOGIC_VECTOR (15 downto 0));
end MUL_104;
architecture Behavioral of MUL_104 is
signal dk_prod, dk_p6, dk_p5, dk_p3: unsigned(15 downto 0);
begin
	dk_p6 <= "00" & unsigned(DK_A) & "000000";
	dk_p5 <= "000" & unsigned(DK_A) & "00000";
	dk_p3 <= "00000" & unsigned(DK_A) & "000";
dk_prod <= dk_p6 + dk_p5 + dk_p3;
DK_Opt <= std_logic_vector (dk_prod);

end Behavioral;
