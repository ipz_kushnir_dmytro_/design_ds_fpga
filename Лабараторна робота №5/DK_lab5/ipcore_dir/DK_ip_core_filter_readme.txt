The following files were generated for 'DK_ip_core_filter' in directory
D:\Xilinx\DK_lab5\ipcore_dir\

Opens the IP Customization GUI:
   Allows the user to customize or recustomize the IP instance.

   * DK_ip_core_filter.mif

XCO file generator:
   Generate an XCO file for compatibility with legacy flows.

   * DK_ip_core_filter.xco

Creates an implementation netlist:
   Creates an implementation netlist for the IP.

   * DK_ip_core_filter.ngc
   * DK_ip_core_filter.vhd
   * DK_ip_core_filter.vho
   * DK_ip_core_filterCOEFF_auto0_0.mif
   * DK_ip_core_filterCOEFF_auto0_1.mif
   * DK_ip_core_filterCOEFF_auto0_2.mif
   * DK_ip_core_filterCOEFF_auto0_3.mif
   * DK_ip_core_filterCOEFF_auto0_4.mif
   * DK_ip_core_filterCOEFF_auto0_5.mif
   * DK_ip_core_filterfilt_decode_rom.mif

Creates an HDL instantiation template:
   Creates an HDL instantiation template for the IP.

   * DK_ip_core_filter.vho

IP Symbol Generator:
   Generate an IP symbol based on the current project options'.

   * DK_ip_core_filter.asy
   * DK_ip_core_filter.mif

SYM file generator:
   Generate a SYM file for compatibility with legacy flows

   * DK_ip_core_filter.sym

Generate ISE metadata:
   Create a metadata file for use when including this core in ISE designs

   * DK_ip_core_filter_xmdf.tcl

Generate ISE subproject:
   Create an ISE subproject for use when including this core in ISE designs

   * DK_ip_core_filter.gise
   * DK_ip_core_filter.xise

Deliver Readme:
   Readme file for the IP.

   * DK_ip_core_filter_readme.txt

Generate FLIST file:
   Text file listing all of the output files produced when a customized core was
   generated in the CORE Generator.

   * DK_ip_core_filter_flist.txt

Please see the Xilinx CORE Generator online help for further details on
generated files and how to use them.

