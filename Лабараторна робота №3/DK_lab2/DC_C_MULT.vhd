----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:00:01 09/18/2016 
-- Design Name: 
-- Module Name:    DC_C_MULT - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity NotOptimized is
Port ( DK_A : in STD_LOGIC_VECTOR (7 downto 0);
DK_C_M : out STD_LOGIC_VECTOR (39 downto 0));
end NotOptimized;
architecture Behavioral of NotOptimized is
signal dk_prod,dk_p30,dk_p29,dk_p28,dk_p27,dk_p26,dk_p24,dk_p23,dk_p22,dk_p21,dk_p20,dk_p19,dk_p18,
dk_p17,dk_p16,dk_p15,dk_p14,dk_p13,dk_p12,dk_p11,dk_p9,dk_p7,dk_p5,dk_p2,
dk_p1,dk_p0:unsigned(39 downto 0); begin
dk_p0  <= "00000000000000000000000000000000" & unsigned(DK_A);
dk_p1  <= "0000000000000000000000000000000" & unsigned(DK_A) & "0";
dk_p2  <= "000000000000000000000000000000" & unsigned(DK_A) & "00";
dk_p5  <= "000000000000000000000000000" & unsigned(DK_A) & "00000";
dk_p7  <= "0000000000000000000000000" & unsigned(DK_A) & "0000000";
dk_p9  <= "00000000000000000000000" & unsigned(DK_A) & "000000000";
dk_p11 <= "000000000000000000000" & unsigned(DK_A) & "00000000000";
dk_p12 <= "00000000000000000000" & unsigned(DK_A) & "000000000000";
dk_p13 <= "0000000000000000000" & unsigned(DK_A) & "0000000000000";
dk_p14 <= "000000000000000000" & unsigned(DK_A) & "00000000000000";
dk_p15 <= "00000000000000000" & unsigned(DK_A) & "000000000000000";
dk_p16 <= "0000000000000000" & unsigned(DK_A) & "0000000000000000";
dk_p17 <= "000000000000000" & unsigned(DK_A) & "00000000000000000";
dk_p18 <= "00000000000000" & unsigned(DK_A) & "000000000000000000";
dk_p19 <= "0000000000000" & unsigned(DK_A) & "0000000000000000000";
dk_p20 <= "000000000000" & unsigned(DK_A) & "00000000000000000000";
dk_p21 <= "00000000000" & unsigned(DK_A) & "000000000000000000000";
dk_p22 <= "0000000000" & unsigned(DK_A) & "0000000000000000000000";
dk_p23 <= "000000000" & unsigned(DK_A) & "00000000000000000000000";
dk_p24 <= "00000000" & unsigned(DK_A) & "000000000000000000000000";
dk_p26 <= "000000" & unsigned(DK_A) & "00000000000000000000000000";
dk_p27 <= "00000" & unsigned(DK_A) & "000000000000000000000000000";
dk_p28 <= "0000" & unsigned(DK_A) & "0000000000000000000000000000";
dk_p29 <= "000" & unsigned(DK_A) & "00000000000000000000000000000";
dk_p30 <= "00" & unsigned(DK_A) & "000000000000000000000000000000";

dk_prod <= (dk_p0+dk_p5+dk_p12+dk_p13+dk_p14+dk_p15+dk_p18+dk_p21+dk_p22+dk_p24+
dk_p26+dk_p27+dk_p28+dk_p29+dk_p30);
DK_C_M <= std_logic_vector (dk_prod);
end Behavioral;
