<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="virtex7" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <attr value="Never" name="RenameBusIO" />
    <netlist>
        <signal name="DK_A(7:0)" />
        <signal name="DK_B(7:0)" />
        <signal name="DK_PROD(15:0)" />
        <signal name="DK_mPROD(15:0)" />
        <signal name="DK_IP_PROD(15:0)" />
        <signal name="DK_CM(39:0)" />
        <signal name="DK_CmM(39:0)" />
        <signal name="DK_IP_CM(38:0)" />
        <port polarity="Input" name="DK_A(7:0)" />
        <port polarity="Input" name="DK_B(7:0)" />
        <port polarity="Output" name="DK_PROD(15:0)" />
        <port polarity="Output" name="DK_mPROD(15:0)" />
        <port polarity="Output" name="DK_IP_PROD(15:0)" />
        <port polarity="Output" name="DK_CM(39:0)" />
        <port polarity="Output" name="DK_CmM(39:0)" />
        <port polarity="Output" name="DK_IP_CM(38:0)" />
        <blockdef name="DK_Multiplier2">
            <timestamp>2016-9-18T5:45:23</timestamp>
            <rect width="320" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="384" y="-108" height="24" />
            <line x2="448" y1="-96" y2="-96" x1="384" />
        </blockdef>
        <blockdef name="DK_Multiplier1">
            <timestamp>2016-9-18T5:47:19</timestamp>
            <rect width="304" x="64" y="-128" height="128" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="368" y="-108" height="24" />
            <line x2="432" y1="-96" y2="-96" x1="368" />
        </blockdef>
        <blockdef name="NotOptimized">
            <timestamp>2016-9-18T14:7:23</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="Optimized">
            <timestamp>2016-9-18T14:10:27</timestamp>
            <rect width="256" x="64" y="-64" height="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="DC_C_IP_MULT">
            <timestamp>2016-9-18T14:49:27</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <blockdef name="DC_IP_MULTIPLIER">
            <timestamp>2016-9-20T5:9:15</timestamp>
            <rect width="512" x="32" y="32" height="384" />
            <line x2="32" y1="80" y2="80" style="linewidth:W" x1="0" />
            <line x2="32" y1="144" y2="144" style="linewidth:W" x1="0" />
            <line x2="544" y1="80" y2="80" style="linewidth:W" x1="576" />
        </blockdef>
        <block symbolname="DK_Multiplier2" name="XLXI_10">
            <blockpin signalname="DK_A(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="DK_B(7:0)" name="DK_B(7:0)" />
            <blockpin signalname="DK_mPROD(15:0)" name="DK_mPROD(15:0)" />
        </block>
        <block symbolname="DK_Multiplier1" name="XLXI_13">
            <blockpin signalname="DK_A(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="DK_B(7:0)" name="DK_B(7:0)" />
            <blockpin signalname="DK_PROD(15:0)" name="DK_PROD(15:0)" />
        </block>
        <block symbolname="NotOptimized" name="XLXI_14">
            <blockpin signalname="DK_A(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="DK_CM(39:0)" name="DK_C_M(39:0)" />
        </block>
        <block symbolname="Optimized" name="XLXI_15">
            <blockpin signalname="DK_A(7:0)" name="DK_A(7:0)" />
            <blockpin signalname="DK_CmM(39:0)" name="DK_Opt(39:0)" />
        </block>
        <block symbolname="DC_C_IP_MULT" name="XLXI_17">
            <blockpin signalname="DK_A(7:0)" name="a(7:0)" />
            <blockpin signalname="DK_IP_CM(38:0)" name="p(38:0)" />
        </block>
        <block symbolname="DC_IP_MULTIPLIER" name="XLXI_18">
            <blockpin signalname="DK_A(7:0)" name="a(7:0)" />
            <blockpin signalname="DK_B(7:0)" name="b(7:0)" />
            <blockpin signalname="DK_IP_PROD(15:0)" name="p(15:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1024" y="944" name="XLXI_10" orien="R0">
        </instance>
        <instance x="1040" y="624" name="XLXI_13" orien="R0">
        </instance>
        <iomarker fontsize="28" x="608" y="528" name="DK_A(7:0)" orien="R180" />
        <branch name="DK_A(7:0)">
            <wire x2="832" y1="528" y2="528" x1="608" />
            <wire x2="1040" y1="528" y2="528" x1="832" />
            <wire x2="832" y1="528" y2="848" x1="832" />
            <wire x2="832" y1="848" y2="1184" x1="832" />
            <wire x2="848" y1="1184" y2="1184" x1="832" />
            <wire x2="1056" y1="1184" y2="1184" x1="848" />
            <wire x2="848" y1="1184" y2="1776" x1="848" />
            <wire x2="848" y1="1776" y2="1936" x1="848" />
            <wire x2="848" y1="1936" y2="2096" x1="848" />
            <wire x2="1104" y1="2096" y2="2096" x1="848" />
            <wire x2="1184" y1="1936" y2="1936" x1="848" />
            <wire x2="1184" y1="1776" y2="1776" x1="848" />
            <wire x2="1024" y1="848" y2="848" x1="832" />
        </branch>
        <branch name="DK_B(7:0)">
            <wire x2="672" y1="592" y2="592" x1="608" />
            <wire x2="1024" y1="592" y2="592" x1="672" />
            <wire x2="1040" y1="592" y2="592" x1="1024" />
            <wire x2="672" y1="592" y2="912" x1="672" />
            <wire x2="1024" y1="912" y2="912" x1="672" />
            <wire x2="672" y1="912" y2="1248" x1="672" />
            <wire x2="1056" y1="1248" y2="1248" x1="672" />
        </branch>
        <iomarker fontsize="28" x="608" y="592" name="DK_B(7:0)" orien="R180" />
        <branch name="DK_PROD(15:0)">
            <wire x2="1504" y1="528" y2="528" x1="1472" />
        </branch>
        <iomarker fontsize="28" x="1504" y="528" name="DK_PROD(15:0)" orien="R0" />
        <branch name="DK_mPROD(15:0)">
            <wire x2="1504" y1="848" y2="848" x1="1472" />
        </branch>
        <iomarker fontsize="28" x="1504" y="848" name="DK_mPROD(15:0)" orien="R0" />
        <branch name="DK_IP_PROD(15:0)">
            <wire x2="1664" y1="1184" y2="1184" x1="1632" />
        </branch>
        <iomarker fontsize="28" x="1664" y="1184" name="DK_IP_PROD(15:0)" orien="R0" />
        <instance x="1184" y="1968" name="XLXI_15" orien="R0">
        </instance>
        <instance x="1184" y="1808" name="XLXI_14" orien="R0">
        </instance>
        <branch name="DK_CM(39:0)">
            <wire x2="1600" y1="1776" y2="1776" x1="1568" />
        </branch>
        <iomarker fontsize="28" x="1600" y="1776" name="DK_CM(39:0)" orien="R0" />
        <branch name="DK_CmM(39:0)">
            <wire x2="1600" y1="1936" y2="1936" x1="1568" />
        </branch>
        <iomarker fontsize="28" x="1600" y="1936" name="DK_CmM(39:0)" orien="R0" />
        <branch name="DK_IP_CM(38:0)">
            <wire x2="1712" y1="2096" y2="2096" x1="1680" />
        </branch>
        <iomarker fontsize="28" x="1712" y="2096" name="DK_IP_CM(38:0)" orien="R0" />
        <instance x="1104" y="2016" name="XLXI_17" orien="R0">
        </instance>
        <instance x="1056" y="1104" name="XLXI_18" orien="R0">
        </instance>
    </sheet>
</drawing>