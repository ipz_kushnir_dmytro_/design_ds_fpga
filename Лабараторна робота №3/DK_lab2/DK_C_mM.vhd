----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:07:48 09/18/2016 
-- Design Name: 
-- Module Name:    DK_C_mM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Optimized is
Port ( DK_A : in STD_LOGIC_VECTOR (7 downto 0);
DK_Opt : out STD_LOGIC_VECTOR (39 downto 0));
end Optimized;
architecture Behavioral of Optimized is
signal dk_prod, 	dk_p31, dk_p30,dk_p29,dk_p28,dk_p27,dk_p26,dk_p25,dk_p24,dk_p23,dk_p22,dk_p21,
dk_p20,dk_p19,dk_p18,dk_p17,dk_p16,dk_p15,dk_p14,dk_p13,dk_p12,dk_p11,dk_p9,dk_p7,dk_p5,dk_p4,
 dk_p3,dk_p2,dk_p1,dk_p0:unsigned(39 downto 0);
begin
dk_p0  <= "00000000000000000000000000000000" & unsigned(DK_A);
dk_p3  <= "00000000000000000000000000000" & unsigned(DK_A) & "000";
dk_p5  <= "000000000000000000000000000" & unsigned(DK_A) & "00000";
dk_p7  <= "0000000000000000000000000" & unsigned(DK_A) & "0000000";
dk_p9  <= "00000000000000000000000" & unsigned(DK_A) & "000000000";
dk_p11 <= "000000000000000000000" & unsigned(DK_A) & "00000000000";
dk_p12 <= "00000000000000000000" & unsigned(DK_A) & "000000000000";
dk_p14 <= "000000000000000000" & unsigned(DK_A) & "00000000000000";
dk_p16 <= "0000000000000000" & unsigned(DK_A) & "0000000000000000";
dk_p18 <= "00000000000000" & unsigned(DK_A) & "000000000000000000";
dk_p21 <= "00000000000" & unsigned(DK_A) & "000000000000000000000";
dk_p22 <= "0000000000" & unsigned(DK_A) & "0000000000000000000000";
dk_p24 <= "00000000" & unsigned(DK_A) & "000000000000000000000000";
dk_p25 <= "0000000" & unsigned(DK_A) & "0000000000000000000000000";
dk_p26 <= "000000" & unsigned(DK_A) & "00000000000000000000000000";
dk_p31 <= "0" & unsigned(DK_A) & "0000000000000000000000000000000";

dk_prod <= (dk_p31+dk_p24+dk_p22+dk_p21+dk_p18+dk_p16+dk_p5+dk_p0)-dk_p26-dk_p12;
DK_Opt <= std_logic_vector (dk_prod);

end Behavioral;
